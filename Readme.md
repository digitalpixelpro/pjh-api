### Installing

You need to install the following to get up and running:
Ruby (you can use rvm or rbenv to install a recent version)
Postgres (I use brew on a mac)
Bundler
Rails

### Running

1. Prior to initial run:
  * Make sure Postgresql is installed
    * There is a `postgres` user with the password of `postgres`
2. Initial run:
  * Get the gems for the project
    * `bundle install`
  * Create the database
    * `rake db:create`
  * Migrate the database
    * `rake db:migrate`
3. Start server
  * `rails server`