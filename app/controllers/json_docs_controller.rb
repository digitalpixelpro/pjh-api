class JsonDocsController < ApplicationController
  before_action :set_json_doc, only: [:show, :edit, :update, :destroy]

  # GET /json_docs
  # GET /json_docs.json
  def index
    @json_docs = JsonDoc.all

    respond_to do |format|
      # format.html # index.html.erb
      format.json { render json: @json_docs, root: :json_docs }
    end
  end

  # GET /json_docs/1
  # GET /json_docs/1.json
  def show
    respond_to do |format|
      # format.html # show.html.erb
      format.json { render json: @json_doc, root: :json_docs }
    end
  end

  # POST /json_docs
  # POST /json_docs.json
  def create
    @json_doc = JsonDoc.new(json_doc_params)

    respond_to do |format|
      if @json_doc.save
        format.json { render json: @json_doc, status: :created, root: :json_docs }
      else
        format.json { render json: @json_doc.errors, root: :json_docs, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /json_docs/1
  # PATCH/PUT /json_docs/1.json
  def update
    respond_to do |format|
      if @json_doc.update(json_doc_params)
        format.json { render json: @json_doc, status: :ok, root: :json_docs }
      else
        format.json { render json: @json_doc.errors, root: :json_docs, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /json_docs/1
  # DELETE /json_docs/1.json
  def destroy
    @json_doc.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_json_doc
      @json_doc = JsonDoc.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def json_doc_params
      params.require(:json_doc).permit(:id, :title, :description, :body)
    end
end
