class JsonDocSerializer < ActiveModel::Serializer
  embed :ids, embed_in_root: true

  attributes :id, :title, :description, :body
end
