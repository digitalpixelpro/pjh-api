class CreateJsonDocs < ActiveRecord::Migration
  def change
    create_table :json_docs do |t|
      t.json :body
      t.string :title
      t.text :description
      t.timestamps null: false
    end
  end
end
